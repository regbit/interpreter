import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Interpreter {
	
	private HashMap<String, Integer> varMap;
	private static Scanner sc;
	
	public Interpreter(String line) {
		Lexer l = new Lexer(line);
		new Parser(l.getTokens());
		
		varMap = new HashMap<String, Integer>();
		
		System.out.println("___________________");
		System.out.println("INTERPRETER");
		execute(l.getTokens());
	}
	
	private void execute(ArrayList<Unit> units) {
		//Terminal
		Terminal term = null;
		for(Unit u : units) {
			if(u.getClass() == Terminal.class) {
				term = (Terminal)u;
				if(u.getType() == Terminal.STATEMENT) {
					//Statement handling
					execute(term.getUnits());
				} else if(term.getType() == Terminal.VAR_INIT || term.getType() == Terminal.VAR_DEF) {
					//Variable initialization handling
					String name = term.getUnits().get(1).getValue();
					int value = term.getUnits().size() < 4 ? 0 : math(term.getUnits().get(3));
					if(!varMap.containsKey(name)) {
						varMap.put(name, value);
						System.out.println("Variable " + name + " initialized with value: " + value);
					} else {
						System.out.println("Error: Variable \"" + name + "\" already exists");
						System.out.println(varMap.get(name));
						System.exit(0);
						return;
					}
				} else if(term.getType() == Terminal.VAR_ASSIGN) {
					//Variable assigning handling
					String name = term.getUnits().get(0).getValue();
					int value = math(term.getUnits().get(2));
					if(varMap.containsKey(name)) {
						varMap.put(name, value);
					} else {
						System.out.println("Error: Variable \"" + name + "\" doesin't exist");
						System.exit(0);
						return;
					}
				} else if(term.getType() == Terminal.MATH) {
					//Math
					math(term);
				} else {
					System.out.println("YEA DUDE");
					System.out.println(term.getType());
					System.exit(0);
				}
			} else {
				//TODO Error handler
			}
		}
	}
	
	private int math(Unit u) {
		switch(u.getType()) {
		case Token.CONST:
			return Integer.parseInt(u.getValue());
		case Token.VARIABLE:
			if(varMap.containsKey(u.getValue())) {
				return varMap.get(u.getValue());
			} else {
				System.out.println("Error: Variable \"" + u.getValue() + "\" doesin't exist");
				System.exit(0);
				return 0;
			}
		case Terminal.MATH:
			if(((Terminal)u).getUnits().get(1).getType() != Terminal.MATH) {
				System.out.println("\nCalculating...");
				int a = 0, b = 0;
				Unit u1 = ((Terminal)u).getUnits().get(0), u2 = ((Terminal)u).getUnits().get(2);
				String sign = ((Terminal)u).getUnits().get(1).getValue();
				System.out.print(u1.getValue() + " " + sign + " " + u2.getValue());
				a = math(u1);
				b = math(u2);
				
				if(sign.contains("+")) {
					System.out.print(" = " + (a + b) + "\n");
					return (a + b);
				} else if(sign.contains("-")) {
					System.out.print(" = " + (a - b) + "\n");
					return (a - b);
				} else if(sign.contains("*")) {
					System.out.print(" = " + (a * b) + "\n");
					return (a * b);
				} else if(sign.contains("/")) {
					System.out.print(" = " + (a / b) + "\n");
					return (a / b);
				} else if(sign.contains("^")) {
					System.out.print(" = " + (int)Math.pow(a, b) + "\n");
					return (int)Math.pow(a, b);
				}
			} else {
				return math(((Terminal)u).getUnits().get(1));
			}
		}
		return 0;
	}
	
	public static void main(String[] args) {
		sc = new Scanner(System.in);
		new Interpreter(sc.nextLine());
	}
}
