import java.util.ArrayList;


public class Terminal implements Unit {
	
	public static final int MATH = 100, VAR_INIT = 101, VAR_DEF = 102, VAR_ASSIGN = 103, STATEMENT = 104, NULL = 105;
	private int type;
	private ArrayList<Unit> units;
	
	public Terminal(int type, ArrayList<Unit> units) {
		this.type = type;
		this.units = units;
	}
	
	public void add(Unit unit) {
		units.add(unit);
	}
	
	public ArrayList<Unit> getUnits() {
		return units;
	}

	public int getType() {
		return type;
	}

	public String getTypeName() {
		switch(type) {
		case MATH:
			return "Math";
		case VAR_INIT:
			return "Variable initialization";
		case VAR_DEF:
			return "Variable definition";
		case VAR_ASSIGN:
			return "Variable assignment";
		case STATEMENT:
			return "Statement";
		default:
			return "Null";
		}
	}

	public String getValue() {
		String value = "";
		for(Unit u : units) {
			value = value.concat(u.getValue() + " ");
		}
		return value;
	}

	public static String getTypeName(int type) {
		switch(type) {
		case MATH:
			return "Math";
		case VAR_INIT:
			return "Variable initialization";
		case VAR_DEF:
			return "Variable definition";
		case VAR_ASSIGN:
			return "Variable assignment";
		case STATEMENT:
			return "Statement";
		default:
			return "Null";
		}
	}
}
