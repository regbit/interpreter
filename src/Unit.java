
public interface Unit {
	
	public int getType();
	public String getTypeName();
	public String getValue();
	
}
