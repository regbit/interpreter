import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Lexer {
	
	private ArrayList<Pattern> patterns;
	private ArrayList<Unit> units;
	
	public Lexer(String line) {
		patterns = new ArrayList<Pattern>(10);
		
		//Key Words
		patterns.add(Pattern.compile("var"));
		//Variables and constants
		patterns.add(Pattern.compile("[a-zA-Z]{1}[a-zA-Z0-9]*"));
		patterns.add(Pattern.compile("([1-9]{1}[0-9]*)|0"));
		//Signs
		patterns.add(Pattern.compile(";"));
		patterns.add(Pattern.compile("="));
		patterns.add(Pattern.compile("[\\*\\/\\^]"));
		patterns.add(Pattern.compile("[\\+\\-]"));
		patterns.add(Pattern.compile("\\("));
		patterns.add(Pattern.compile("\\)"));
		//Space check
		patterns.add(Pattern.compile("\\s+"));
		
		units = new ArrayList<Unit>();
		
		analyze(line);
		
		for(Unit u : units) {
			System.out.print(u.getValue() + " ");
		}
		System.out.println();
	}
	
	private void analyze(String line) {
		System.out.println("___________________");
		System.out.println("LEXER");
		
		//Bounds
		int from = 0, to = 1;
		//Currently analyzing string
		String analyzing = null;
		//Matcher
		Matcher m;
		//Potential token type
		int highest = Token.NULL;
		//At least 1 match
		boolean match = false;
		
		while(from < line.length() && to < line.length() + 1) {
			analyzing = line.substring(from, to);
			System.out.println("Analizing: \"" + analyzing + "\"");
			
			//Going through all the patterns with matcher
			for(int i = 0; i < patterns.size(); i++) {
				Pattern p = patterns.get(i);
				
				m = p.matcher(analyzing);
				
				if(m.matches()) {
					match = true;
					if(i <= highest) {
						System.out.println("Match: " + Token.getTypeName(i));
						highest = i;
						break;
					}
				}
			}
			
			//Matcher results handler
			if(!match) {
				if(highest != Token.NULL) {
					units.add(new Token(highest, line.substring(from, to - 1)));
					System.out.println("Added token: " + units.get(units.size() - 1).getTypeName());
					highest = Token.NULL;
					from = to - 1;
				} else {
					System.out.println("Space");
					from = to - 1;
				}
			} else {
				if(to == line.length()) {
					units.add(new Token(highest, line.substring(from, to)));
					System.out.println("Added token: " + units.get(units.size() - 1).getTypeName());
					highest = Token.NULL;
				}
				to++;
			}
			
			System.out.println();
			match = false;
		}
	}
	
	public ArrayList<Unit> getTokens() {
		return units;
	}
}
