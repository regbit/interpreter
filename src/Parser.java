import java.util.ArrayList;

public class Parser {
	
	public Parser(ArrayList<Unit> units) {
		analyze(units);
	}
	
	private void analyze(ArrayList<Unit> units) {
		System.out.println("___________________");
		System.out.println("PARSER");
		ArrayList<Unit> subList = new ArrayList<Unit>();
		//Math
		if(units.size() > 2) {
			for(int j = 0; j < 4; j++) {
				for(int i = 0; i + 2 < units.size(); i++) {
					subList = new ArrayList<Unit>(units.subList(i, i + 3));
					Unit u = null;
					switch(j) {
					case 0:
						u = sign(subList);
						break;
					case 1:
						u = mathHigh(subList);
						break;
					case 2:
						u = mathLow(subList);
						break;
					case 3:
						u = brackets(subList);
						break;
					}
					
					if(u != null) {
						System.out.println("Added: " + u.getTypeName() + ": \"" + u.getValue() + "\"");
						units.remove(i);
						units.remove(i);
						units.set(i, u);
						i = -1;
						j = 0;
					}
				}
			}
		}
		
		//Variable initialization
		if(units.size() > 3) {
			for(int i = 0; i + 3 < units.size(); i++) {
				subList = new ArrayList<Unit>(units.subList(i, i + 4));
				Unit u = variableInit(subList);
				if(u != null) {
					System.out.println("Added: " + u.getTypeName() + ": \"" + u.getValue() + "\"");
					units.remove(i);
					units.remove(i);
					units.remove(i);
					units.set(i, u);
					i = -1;
				}
			}
		}
		
		//Variable definition
		if(units.size() > 1) {
			for(int i = 0; i + 1 < units.size(); i++) {
				subList = new ArrayList<Unit>(units.subList(i, i + 2));
				Unit u = variableDef(subList);
				if(u != null) {
					System.out.println("Added: " + u.getTypeName() + ": \"" + u.getValue() + "\"");
					units.remove(i);
					units.set(i, u);
					i = -1;
				}
			}
		}
		
		//Variable assign
		if(units.size() > 2) {
			for(int i = 0; i + 2 < units.size(); i++) {
				subList = new ArrayList<Unit>(units.subList(i, i + 3));
				Unit u = variableAssign(subList);
				if(u != null) {
					System.out.println("Added: " + u.getTypeName() + ": \"" + u.getValue() + "\"");
					units.remove(i);
					units.remove(i);
					units.set(i, u);
					i = -1;
				}
			}
		}
		
		//Statement
		if(units.size() > 1) {
			for(int i = 0; i + 1 < units.size(); i++) {
				subList = new ArrayList<Unit>(units.subList(i, i + 2));
				Unit u = statement(subList);
				if(u != null) {
					System.out.println("Added: " + u.getTypeName() + ": \"" + u.getValue() + "\"");
					units.remove(i);
					units.set(i, u);
					i = -1;
				}
			}
		}
	}
	
	private Unit sign(ArrayList<Unit> units) {
		//Units reference
		Unit u1 = units.get(0), u2 = units.get(1), u3 = units.get(2);
		
		if(!(u1.getType() == Token.CONST || u1.getType() == Token.VARIABLE || u1.getType() == Token.RIGHT_BRACKET || u1.getType() == Terminal.MATH)) {
			if(u2.getType() == Token.MATH_LOW) {
				if(u3.getType() == Token.CONST || u3.getType() == Token.VARIABLE || u3.getType() == Terminal.MATH) {
					return new Terminal(Terminal.MATH, units);
				}
			}
		}
		return null;
	}
	
	private Unit mathHigh(ArrayList<Unit> units) {
		//Units reference
		Unit u1 = units.get(0), u2 = units.get(1), u3 = units.get(2);
		
		if(u1.getType() == Token.CONST || u1.getType() == Token.VARIABLE || u1.getType() == Terminal.MATH) {
			if(u2.getType() == Token.MATH_HIGH) {
				if(u3.getType() == Token.CONST || u3.getType() == Token.VARIABLE || u3.getType() == Terminal.MATH) {
					return new Terminal(Terminal.MATH, units);
				}
			}
		}
		return null;
	}
	
	private Unit mathLow(ArrayList<Unit> units) {
		//Units reference
		Unit u1 = units.get(0), u2 = units.get(1), u3 = units.get(2);
		
		if(u1.getType() == Token.CONST || u1.getType() == Token.VARIABLE || u1.getType() == Terminal.MATH) {
			if(u2.getType() == Token.MATH_LOW) {
				if(u3.getType() == Token.CONST || u3.getType() == Token.VARIABLE || u3.getType() == Terminal.MATH) {
					return new Terminal(Terminal.MATH, units);
				}
			}
		}
		return null;
	}
	
	private Unit brackets(ArrayList<Unit> units) {
		//Units reference
		Unit u1 = units.get(0), u2 = units.get(1), u3 = units.get(2);
		
		if(u1.getType() == Token.LEFT_BRACKET) {
			if(u2.getType() == Terminal.MATH) {
				if(u3.getType() == Token.RIGHT_BRACKET) {
					return new Terminal(Terminal.MATH, units);
				}
			}
		}
		return null;
	}
	
	private Unit variableInit(ArrayList<Unit> units) {
		//Units reference
		Unit u1 = units.get(0), u2 = units.get(1), u3 = units.get(2), u4 = units.get(3);
		
		if(u1.getType() == Token.KEY_VAR) {
			if(u2.getType() == Token.VARIABLE) {
				if(u3.getType() == Token.ASSIGNMENT) {
					if(u4.getType() == Token.CONST || u4.getType() == Token.VARIABLE || u4.getType() == Terminal.MATH) {
						return new Terminal(Terminal.VAR_INIT, units);
					}
				}
			}
		}
		return null;
	}
	
	private Unit variableDef(ArrayList<Unit> units) {
		//Units reference
		Unit u1 = units.get(0), u2 = units.get(1);
		if(u1.getType() == Token.KEY_VAR) {
			if(u2.getType() == Token.VARIABLE) {
				return new Terminal(Terminal.VAR_DEF, units);
			}
		}
		return null;
	}
	
	private Unit variableAssign(ArrayList<Unit> units) {
		//Units reference
		Unit u1 = units.get(0), u2 = units.get(1), u3 = units.get(2);
		
		if(u1.getType() == Token.VARIABLE) {
			if(u2.getType() == Token.ASSIGNMENT) {
				if(u3.getType() == Token.CONST || u3.getType() == Token.VARIABLE || u3.getType() == Terminal.MATH) {
					return new Terminal(Terminal.VAR_ASSIGN, units);
				}
			}
		}
		return null;
	}
	
	private Unit statement(ArrayList<Unit> units) {
		//Units reference
		Unit u1 = units.get(0), u2 = units.get(1);
		
		if(u1.getType() == Terminal.VAR_INIT || u1.getType() == Terminal.VAR_DEF || u1.getType() == Terminal.VAR_ASSIGN) {
			if(u2.getType() == Token.END_OF_LINE) {
				return new Terminal(Terminal.STATEMENT, units);
			}
		}
		return null;
	}
}
