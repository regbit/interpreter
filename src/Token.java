
public class Token implements Unit {

	public static final int KEY_VAR = 0, VARIABLE = 1, CONST = 2, END_OF_LINE = 3, ASSIGNMENT = 4, MATH_HIGH = 5, MATH_LOW = 6, LEFT_BRACKET = 7, RIGHT_BRACKET = 8, NULL = 9;
	private int type;
	private String value;
	
	public Token(int type, String value) {
		this.type = type;
		this.value = value;
	}
	
	public int getType() {
		return type;
	}

	public String getTypeName() {
		switch(type) {
		case KEY_VAR:
			return "Key var";
		case VARIABLE:
			return "Variable";
		case CONST:
			return "Constant";
		case END_OF_LINE:
			return "End of line";
		case ASSIGNMENT:
			return "Assignment";
		case MATH_HIGH:
			return "Math high priority";
		case MATH_LOW:
			return "Math low priority";
		case LEFT_BRACKET:
			return "Left bracket";
		case RIGHT_BRACKET:
			return "Right bracket";
		default:
			return "Null";
		}
	}

	public String getValue() {
		return value;
	}

	public static String getTypeName(int type) {
		switch(type) {
		case KEY_VAR:
			return "Key var";
		case VARIABLE:
			return "Variable";
		case CONST:
			return "Constant";
		case END_OF_LINE:
			return "End of line";
		case ASSIGNMENT:
			return "Assignment";
		case MATH_HIGH:
			return "Math high priority";
		case MATH_LOW:
			return "Math low priority";
		case LEFT_BRACKET:
			return "Left bracket";
		case RIGHT_BRACKET:
			return "Right bracket";
		default:
			return "Null";
		}
	}
}
